package com.sjj.service;

import com.sjj.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sjj on 2017/3/16.
 */
public interface IUserService {
    public List<User> getAll();
}
