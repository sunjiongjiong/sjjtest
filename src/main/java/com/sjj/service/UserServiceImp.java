package com.sjj.service;

import com.sjj.mapper.UserMapper;
import com.sjj.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sjj on 2017/3/16.
 */
@Service
public class UserServiceImp implements IUserService {

    @Autowired
    private UserMapper userMapper;

    public List<User> getAll() {
        return userMapper.getAll();
    }
}
