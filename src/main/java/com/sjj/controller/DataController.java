package com.sjj.controller;

import com.sjj.model.User;
import com.sjj.service.IUserService;
import com.sjj.service.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sjj on 2017/3/16.
 */
@Controller
public class DataController {

    @Autowired
    private UserServiceImp userServiceImp;

    @RequestMapping(value = "/getData",method = RequestMethod.GET)
    @ResponseBody
    public List<User> getData(ServletRequest req, ServletResponse res){
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", "*");
        List<User> users = userServiceImp.getAll();
        return users;
    }
}
