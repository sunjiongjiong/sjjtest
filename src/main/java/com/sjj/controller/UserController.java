package com.sjj.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * Created by sjj on 2017/3/14.
 */
@Controller
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping("/hello")
    public String hello(){
        logger.info("登陆--->用户信息 用户名{}，密码{}","sjj",123);
        return "hello";
    }

    @RequestMapping("/login")
    public String login(){
        logger.info("登陆--->用户信息 用户名{}，密码{}","sjj,123");
        return "login";
    }
}
