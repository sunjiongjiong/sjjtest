package com.sjj.mapper;

import com.sjj.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sjj on 2017/3/14.
 */
@Repository
public interface UserMapper {
    public User getOne(int id);
    public void add(User user);
    public List<User> getAll();
    public List<User> getSome(int b,int l);
}
